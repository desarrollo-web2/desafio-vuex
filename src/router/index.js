import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from '../views/Vuex'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Vuex',
    component: Vuex
  },
]

const router = new VueRouter({
  routes
})

export default router
