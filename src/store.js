import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //Variable "globales".
        compras: 0,
        poblacion: 0,
        area: 0,
        paises: [],
        likes: 0,
        agregaciones: [],
        likesArray: [],
        paisesLikeArray: []
    },
    getters: {
        /*Este Getter calcula una nuevo dato a partir
        de las variables establecidas.*/
        promedio(state) {
            if (state.compras === 0) {
                return 0
            }
            return parseInt(state.poblacion / state.compras)
        },
        ultimasAgregaciones(state) {
            return state.agregaciones.slice(Math.max(state.agregaciones.length - 5, 0))
        },
        /*         paisesConLike(state) {
                    for (let i = 0; i < state.paises.length; i++) {
                        if (state.likesArray[i] === true) {
                            state.paisesLikeArray.push(state.paises[i])
                        }
                    }
                    return state.paisesLikeArray
                } */
    },
    mutations: {
        nuevaCompra(state, nuevaPoblacion) {
            state.compras = state.compras + 1
            state.poblacion = state.poblacion + nuevaPoblacion
        },
        nuevaAgregacion(state, pais) {
            state.agregaciones.push({ pais })
        },
        nuevoLike(state, index) {
            state.paisesLikeArray.push(state.paises[index])
        },
        deleteLike(state, index) {
            const indice = state.paisesLikeArray.indexOf(state.paises[index])
            state.paisesLikeArray.splice(indice, 1)
        },
        agregarArea(state, area) {
            state.area = state.area + area
        },
        getPaises(state, paises) {
            state.paises = paises
        },
        setLikes(state, like) {
            if (like) state.likes++
            else state.likes--
        },
        resetAll(state) {
            state.agregaciones = []
            state.likesArray = []
            state.paisesLikeArray = []
            state.area = 0
            state.compras = 0
            state.likes = 0
            state.poblacion = 0
        }
    },

    actions: {
        /*Se crea un action con el objetivo de obtener los países(ConsumirlaAPI),
        mediante un GET*/
        //Metodo Asincronico.
        async getAllCountries(context) {
            return await axios.get('https://restcountries.eu/rest/v2/regionalbloc/usan').then(
                response => {
                    let paises = response.data
                    context.commit('getPaises', paises)
                })
        }
    }
})
